// -------- Service Menu Tabs --------

const $servicesMenu = $(".services__menu");
const $servicesArticle = $(".services__article");

$servicesMenu.on("click", "div",  function(){
    $servicesMenu.find(".services__active").removeClass("services__active");
    $servicesArticle.find(".services__active").removeClass("services__active");
    $(this).addClass("services__active");
    const index = $(this).index();
    $servicesArticle.children().eq(index).addClass("services__active");
});


// -------- Work Filters --------

const $workMenu = $(".work__menu");
const $workImages = $(".work__images-block");

let $workLoaderValue = 12;
$.fn.$workLoader = function () {
    $(".work-active").removeClass("work-active");
    for (let i = 0; i < $workLoaderValue; i++) {
        $(this).eq(i).addClass("work-active");
    }
};

let $selectedWork = $workImages.children();
$selectedWork.$workLoader();

$workMenu.on("click","div", function () {
    $workMenu.find(".work__menu__active").removeClass("work__menu__active");
    $(this).addClass("work__menu__active");

    const selected = $(this).attr("id");
    switch (selected) {
        case "work__all":
            $selectedWork = $workImages.children();
            break;
        case "work__graphic":
            $selectedWork = $(".work__graphic");
            break;
        case "work__web":
            $selectedWork = $(".work__web");
            break;
        case "work__landing":
            $selectedWork = $(".work__landing");
            break;
        case "work__wordpress":
            $selectedWork = $(".work__wordpress");
            break;
    }
    $workLoaderValue = 12;
    workLoaderLength();
    $selectedWork.$workLoader();
});

const $workLoaderButton = $(".work__content .load-more");
function workLoaderLength () {
    if ($selectedWork.length > $workLoaderValue) {
        $workLoaderButton.css("display", "inline-block");
    } else {
        $workLoaderButton.css("display", "none");
    }
}

$workLoaderButton.click( function () {
    $workLoaderButton.css("display", "none");
    $(".work__content .loader").css("display", "inline-block");
    setTimeout(()=> {
        $(".work__content .loader").css("display", "none");
        $workLoaderButton.css("display", "inline-block");
        $workLoaderValue += 12;
        $selectedWork.$workLoader();
        workLoaderLength();
        if ($workLoaderValue >= 36){
            $workLoaderButton.css("display", "none");
        }
    }, 2000);
});

// -------- Work Hover ADD  ---------
// -------- для разнообразия без JQuery ---------

const workPictures = document.querySelectorAll(".work__images-block>div");
let titleCategory;
let category;

workPictures.forEach((element)=> {
    let name = element.className.split(" ", 1)[0];

    switch (name) {
        case "work__graphic":
            titleCategory = "Creative Design";
            category = "Graphic Design";
            break;
        case "work__web":
            titleCategory = "Excellent Experience";
            category = "Web Design";
            break;
        case "work__landing":
            titleCategory = "Great Sales";
            category = "Landing Pages";
            break;
        case "work__wordpress":
            titleCategory = "Faster Development";
            category = "Wordpress";
            break;
    }
    element.insertAdjacentHTML("beforeend",(
    `<div class="work__images-block-hover">
        <div>
            <a href="#"><span class="mdi mdi-link-variant"></span></a>
            <a href="#"><span class="mdi mdi-magnify"></span></a>
        </div>
        <p class="work-hover-title">${titleCategory}</p>
        <p class="work-hover-subtitle">${category}</p>
    </div>`));
});


// --------  Feedback carousel  --------

const $feedbackNav = $(".feedback__navigation-carousel");
const $feedbackContent = $(".feedback__content__carousel");
let activeFeedbackIndex = $feedbackNav.find(".feedback__navigation__active").index() - 1;

function feedbackCarousel () {
    $feedbackNav.find(".feedback__navigation__active").removeClass("feedback__navigation__active");
    $feedbackNav.children().eq(activeFeedbackIndex + 1).addClass("feedback__navigation__active");

    $feedbackContent.find(".feedback__content__active").animate({
        opacity: "0.1",
    }, 400, () => {
        $feedbackContent.find(".feedback__content__active").removeClass("feedback__content__active");
        $feedbackContent.children().eq(activeFeedbackIndex).addClass("feedback__content__active").css({opacity: "0.1"});
        $feedbackContent.find(".feedback__content__active").animate({
            opacity: "1",
        }, 400);
    });
}

$feedbackNav.on("click","img", function () {
    if (!$(this).hasClass("feedback__navigation__active")) {
        activeFeedbackIndex = $(this).index() - 1;
        feedbackCarousel()
    }
});

$feedbackNav.on("click", "div", function () {
    if ($(this).hasClass("feedback__prev")) {
        (activeFeedbackIndex === 0) ? activeFeedbackIndex = $feedbackContent.children().length - 1 : activeFeedbackIndex -= 1;
    } else {
        (activeFeedbackIndex === $feedbackContent.children().length - 1) ? activeFeedbackIndex = 0 :  activeFeedbackIndex += 1;
    }
    feedbackCarousel()
});

// ------------------  Gallery Masonry  ------------

let $galleryLoaderValue = 12;
$.fn.$galleryLoader = function () {
    for (let i = 0; i < $galleryLoaderValue; i++) {
        $(this).eq(i).addClass("gallery-active");
    }
};

const $galleryGrid = $(".gallery__grid").children();
$galleryGrid.$galleryLoader();


const $galleryLoaderButton = $(".gallery .load-more");
function galleryLoaderLength () {
    if ($galleryGrid.length > $galleryLoaderValue) {
        $galleryLoaderButton.css("display", "inline-block");
    } else {
        $galleryLoaderButton.css("display", "none");
    }
}

$galleryLoaderButton.click( function () {
    $galleryLoaderButton.css("display", "none");
    $(".gallery .loader").css("display", "inline-block");
    setTimeout(()=> {
        $(".gallery .loader").css("display", "none");
        $galleryLoaderButton.css("display", "inline-block");
        $galleryLoaderValue += 12;
        $galleryGrid.$galleryLoader();
        galleryLoaderLength();
        if ($galleryLoaderValue >= 36){
            $galleryLoaderButton.css("display", "none");
        }
    }, 2000);
});


$('.gallery__grid').masonry({
    columnWidth: 20,
    itemSelector: '.gallery__grid__item',
    gutter: 12,
});
// wrapper version
// $('.gallery__inner__grid').masonry({
//     columnWidth: 7,
//     itemSelector: '.gallery__inner__grid__item',
//     gutter: 5,
// });

const galleryImages = document.querySelectorAll(".gallery__grid__item");

galleryImages.forEach((element) => {
    element.insertAdjacentHTML("beforeend",(
        `<div class="gallery__grid__item__hover">
            <div>
                <a href="#"><span class="mdi mdi-magnify"></span></a>
                <a href="#"><span class="mdi mdi-arrow-expand-all"></span></a>
            </div>
        </div>`));
});
